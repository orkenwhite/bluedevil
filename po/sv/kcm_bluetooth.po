# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the bluedevil package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: bluedevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-18 00:38+0000\n"
"PO-Revision-Date: 2022-08-02 22:06+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stefan Asserhäll"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "stefan.asserhall@bredband.net"

#: bluetooth.cpp:102
#, kde-format
msgctxt "DeviceName Network (Service)"
msgid "%1 Network (%2)"
msgstr "%1 nätverk (%2)"

#: ui/Device.qml:70 ui/main.qml:237
#, kde-format
msgid "Disconnect"
msgstr "Koppla ner"

#: ui/Device.qml:70 ui/main.qml:237
#, kde-format
msgid "Connect"
msgstr "Anslut"

#: ui/Device.qml:102
#, kde-format
msgid "Type:"
msgstr "Typ:"

#: ui/Device.qml:108
#, kde-format
msgid "%1%"
msgstr ""

#: ui/Device.qml:112
#, kde-format
msgid "Battery:"
msgstr ""

#: ui/Device.qml:117 ui/General.qml:42
#, kde-format
msgid "Address:"
msgstr "Adress:"

#: ui/Device.qml:122
#, kde-format
msgid "Adapter:"
msgstr "Anslutning:"

#: ui/Device.qml:128 ui/General.qml:36
#, kde-format
msgid "Name:"
msgstr "Namn:"

#: ui/Device.qml:132
#, kde-format
msgid "Trusted"
msgstr "Pålitlig"

#: ui/Device.qml:138
#, kde-format
msgid "Blocked"
msgstr "Blockerad"

#: ui/Device.qml:144
#, kde-format
msgid "Send File"
msgstr "Skicka fil"

#: ui/Device.qml:151
#, kde-format
msgid "Setup NAP Network…"
msgstr "Ställ in NAP-nätverk…"

#: ui/Device.qml:158
#, kde-format
msgid "Setup DUN Network…"
msgstr "Ställ in DUN-nätverk…"

#: ui/General.qml:19
#, kde-format
msgid "Settings"
msgstr "Inställningar"

#: ui/General.qml:28
#, kde-format
msgid "Device:"
msgstr "Enhet:"

#: ui/General.qml:46
#, kde-format
msgid "Enabled:"
msgstr "Aktiverad:"

#: ui/General.qml:52
#, kde-format
msgid "Visible:"
msgstr "Synlig:"

#: ui/General.qml:66
#, kde-format
msgid "On login:"
msgstr "Vid inloggning:"

#: ui/General.qml:67
#, kde-format
msgid "Enable Bluetooth"
msgstr "Aktivera Blåtand"

#: ui/General.qml:77
#, kde-format
msgid "Disable Bluetooth"
msgstr "Inaktivera Blåtand"

#: ui/General.qml:87
#, kde-format
msgid "Restore previous status"
msgstr "Återställ föregående status"

#: ui/General.qml:106
#, kde-format
msgid "When receiving files:"
msgstr "När filer tas emot:"

#: ui/General.qml:108
#, kde-format
msgid "Ask for confirmation"
msgstr "Fråga efter bekräftelse"

#: ui/General.qml:117
#, kde-format
msgid "Accept for trusted devices"
msgstr "Acceptera för pålitliga enheter"

#: ui/General.qml:127
#, kde-format
msgid "Always accept"
msgstr "Acceptera alltid"

#: ui/General.qml:137
#, kde-format
msgid "Save files in:"
msgstr "Spara filer i:"

#: ui/General.qml:161 ui/General.qml:172
#, kde-format
msgid "Select folder"
msgstr "Välj katalog"

#: ui/main.qml:27
#, fuzzy, kde-format
#| msgid "Enabled:"
msgctxt "@action: button as in, 'enable Bluetooth'"
msgid "Enabled"
msgstr "Aktiverad:"

#: ui/main.qml:43
#, kde-format
msgid "Add New Device…"
msgstr "Lägg till ny enhet..."

#: ui/main.qml:49
#, kde-format
msgid "Configure…"
msgstr "Anpassa…"

#: ui/main.qml:88
#, kde-format
msgid "Forget this Device?"
msgstr "Glöm enhet?"

#: ui/main.qml:89
#, kde-format
msgid "Are you sure you want to forget \"%1\"?"
msgstr "Är du säker på att du vill glömma '\"%1'?"

#: ui/main.qml:100
#, kde-format
msgctxt "@action:button"
msgid "Forget Device"
msgstr "Glöm enhet"

#: ui/main.qml:107
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Avbryt"

#: ui/main.qml:168
#, kde-format
msgid "No Bluetooth adapters found"
msgstr "Inga Blåtandanslutningar hittades"

#: ui/main.qml:169
#, kde-format
msgid "Connect an external Bluetooth adapter"
msgstr ""

#: ui/main.qml:178
#, kde-format
msgid "Bluetooth is disabled"
msgstr "Blåtand är inte aktiverat"

#: ui/main.qml:184
#, kde-format
msgid "Enable"
msgstr "Aktivera"

#: ui/main.qml:194
#, kde-format
msgid "No devices paired"
msgstr "Inga enheter ihopparade"

#: ui/main.qml:195
#, kde-kuit-format
msgctxt "@info"
msgid "Click <interface>Add New Device…</interface> to pair some"
msgstr ""

#: ui/main.qml:217
#, kde-format
msgid "Connected"
msgstr "Ansluten"

#: ui/main.qml:217
#, kde-format
msgid "Available"
msgstr "Tillgängligt"

#: ui/main.qml:253
#, kde-format
msgctxt "@action:button %1 is the name of a Bluetooth device"
msgid "Forget \"%1\""
msgstr "Glöm \"%1\""

#: ui/main.qml:284
#, kde-format
msgid "%1% Battery"
msgstr ""

#~ msgctxt "This device is a Phone"
#~ msgid "Phone"
#~ msgstr "Telefon"

#~ msgctxt "This device is a Modem"
#~ msgid "Modem"
#~ msgstr "Modem"

#~ msgctxt "This device is a Computer"
#~ msgid "Computer"
#~ msgstr "Dator"

#~ msgctxt "This device is of type Network"
#~ msgid "Network"
#~ msgstr "Nätverk"

#~ msgctxt "This device is a Headset"
#~ msgid "Headset"
#~ msgstr "Headset"

#~ msgctxt "This device is a Headphones"
#~ msgid "Headphones"
#~ msgstr "Hörlurar"

#~ msgctxt "This device is an Audio/Video device"
#~ msgid "Multimedia Device"
#~ msgstr "Multimediaenhet"

#~ msgctxt "This device is a Keyboard"
#~ msgid "Keyboard"
#~ msgstr "Tangentbord"

#~ msgctxt "This device is a Mouse"
#~ msgid "Mouse"
#~ msgstr "Mus"

#~ msgctxt "This device is a Joypad"
#~ msgid "Joypad"
#~ msgstr "Styrspak"

#~ msgctxt "This device is a Graphics Tablet (input device)"
#~ msgid "Tablet"
#~ msgstr "Ritplatta"

#~ msgctxt "This device is a Peripheral device"
#~ msgid "Peripheral"
#~ msgstr "Periferienhet"

#~ msgctxt "This device is a Camera"
#~ msgid "Camera"
#~ msgstr "Kamera"

#~ msgctxt "This device is a Printer"
#~ msgid "Printer"
#~ msgstr "Skrivare"

#~ msgctxt ""
#~ "This device is an Imaging device (printer, scanner, camera, display, …)"
#~ msgid "Imaging"
#~ msgstr "Bildåtergivning"

#~ msgctxt "This device is a Wearable"
#~ msgid "Wearable"
#~ msgstr "Bärbar"

#~ msgctxt "This device is a Toy"
#~ msgid "Toy"
#~ msgstr "Leksak"

#~ msgctxt "This device is a Health device"
#~ msgid "Health"
#~ msgstr "Hälsa"

#~ msgctxt "Type of device: could not be determined"
#~ msgid "Unknown"
#~ msgstr "Okänd"

#~ msgid "Confirm Deletion of Device"
#~ msgstr "Bekräfta borttagning av enhet"

#~ msgid "Remove"
#~ msgstr "Ta bort"

#~ msgid "Bluetooth"
#~ msgstr "Blåtand"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Add…"
#~ msgstr "Lägg till…"

#~ msgctxt "This device is an Audio device"
#~ msgid "Audio"
#~ msgstr "Ljud"

#~ msgid "Add..."
#~ msgstr "Lägg till..."
